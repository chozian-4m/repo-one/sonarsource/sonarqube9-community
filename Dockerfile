ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/openjdk/openjdk11
ARG BASE_TAG=1.11

FROM sonarqube:9.4.0-community AS build

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ARG SONARQUBE_VERSION="9.4-community"
ENV SONAR_VERSION="${SONARQUBE_VERSION}" \
    SONARQUBE_HOME="/opt/sonarqube" \
    SQ_DATA_DIR="/opt/sonarqube/data" \
    SQ_EXTENSIONS_DIR="/opt/sonarqube/extensions" \
    SQ_LOGS_DIR="/opt/sonarqube/logs" \
    SQ_TEMP_DIR="/opt/sonarqube/temp" \
    ES_TMPDIR="/opt/sonarqube/temp"

USER root
RUN set -ex \
    && groupadd -r -g 1000 sonarqube \
    && useradd -r -u 1000 -g sonarqube sonarqube \
    && dnf update -y \
    && dnf install -y fontconfig freetype \
    && dnf clean all \
    && rm -rf /var/cache/dnf \
    && sed --in-place --expression="s?securerandom.source=file:/dev/random?securerandom.source=file:/dev/urandom?g" "${JAVA_HOME}/conf/security/java.security" 

COPY --chmod=755 --chown=sonarqube:sonarqube --from=build ${SONARQUBE_HOME} ${SONARQUBE_HOME}
COPY --chmod=755 --chown=sonarqube:sonarqube scripts/*.sh ${SONARQUBE_HOME}/bin/
COPY --chmod=755 --chown=root:root scripts/docker-healthcheck /usr/local/bin/

RUN set -ex \
    && ln -s ${SONARQUBE_HOME}/lib/sonar-application-*.jar ${SONARQUBE_HOME}/lib/sonar-application-${SONAR_VERSION}.jar 

USER sonarqube
WORKDIR ${SONARQUBE_HOME}
EXPOSE 9000

HEALTHCHECK CMD ["docker-healthcheck"]

STOPSIGNAL SIGINT

ENTRYPOINT ["bin/run.sh"]
CMD ["bin/sonar.sh"]
